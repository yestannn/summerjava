package kz.aitu.oop.practice.practice4;

public class Fishes extends Aquarium {

    public Fishes(String name) {
        super(name);
    }

    public void swim()
    {
        System.out.println("Fish swiming " + 1.5 + "m/s");
    }

    public void eat()
    {
        System.out.println("Fish eats hleb and spec edu");
    }

}

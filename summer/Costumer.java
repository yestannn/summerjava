package kz.aitu.oop.practice.practice4;

public class Costumer {

    private String name;
    private double price = 0.0;
    private int fish = 0;
    private int reptilia = 0;

    public Costumer(String name) {
        this.name = name;
    }
    public String Show(){
        return "fish - " + fish + " reptilia - " + reptilia ;
    }

    public void addMoney(double money) {
        if(money <= 0) return;
        price += money;
    }
    public void addFish(int count){
        if(count <= 0) return;
        fish += count;
    }
    public void addReptilia(int count){
        if(count <= 0) return;
        reptilia += count;
    }
    public void minusMoney(double money) {
        if(money <= 0) return;
        price -= money;
    }


    public double getBalance() {
        return price;
    }
    public String getName() {
        return name;
    }


}
